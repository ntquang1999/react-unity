import React, { useRef, useEffect , useState, useCallback} from 'react';

import UnityView from '@azesmway/react-native-unity';
import { View , Button, ProgressViewIOSComponent, StyleSheet } from 'react-native';
import {Text,TouchableOpacity} from 'react-native';

interface IMessage {
  gameObject: string;
  methodName: string;
  message: string;
}

const Unity = (props) => {
  const unityRef = useRef(null);


  useEffect(() => {
    if (unityRef?.current) {
      const message: IMessage = {
        gameObject: 'gameObject',
        methodName: 'methodName',
        message: 'message',
      };
      unityRef.current.postMessage(message.gameObject, message.methodName, message.message);
    }
  }, []);

  return (
    <View style={{ flex: 1 }}>
      <UnityView
        ref={unityRef}
        style={{ flex: 1 }}
        onUnityMessage={(result) => {
          if(result.nativeEvent.message == "exit")
            props.onExit();
            
        }}
      />
    </View>
  );


}

const App = () => {
  const [show, setShow] = useState(false);
  if (show) {
    return (
      <Unity onExit = {()=> setShow(false)} />
    )
  }
  return (
    <View style = {styles.container}>
      <Button title={'Open Unity'} onPress={() => setShow(true)} color="#841584" />
    </View>
    
  )
}

var styles = StyleSheet.create({

  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    backgroundColor: "cyan",
    padding: 20,
    margin: 10,
  },


});



export default App;